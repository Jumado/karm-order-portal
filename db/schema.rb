# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140616152025) do

  create_table "ALU_ISSUES", primary_key: "ID", force: true do |t|
    t.string   "Issueno",   limit: 50
    t.string   "ITEM_CODE", limit: 50
    t.float    "QTY",       limit: 53
    t.datetime "DATE"
    t.integer  "month"
    t.integer  "year"
  end

  create_table "ALU_RECEIPTS", primary_key: "ID", force: true do |t|
    t.text     "GRNO"
    t.string   "ITEM_CODE", limit: 50
    t.float    "QTY",       limit: 53
    t.datetime "DATE"
    t.integer  "month"
    t.integer  "year"
  end

  create_table "AUTOSEND", primary_key: "ID", force: true do |t|
    t.string   "username",     limit: 100
    t.string   "userphone",    limit: 30
    t.datetime "date1"
    t.datetime "lastnotified"
    t.string   "status",       limit: 30
  end

  create_table "Allocated_Blanking", force: true do |t|
    t.string   "Item_code",        limit: 50
    t.text     "item_description"
    t.float    "qty",              limit: 53
    t.string   "batchno",          limit: 50
    t.string   "username",         limit: 50
    t.datetime "date"
  end

  create_table "Allocated_slits", force: true do |t|
    t.integer  "batchno",          limit: 8
    t.string   "slit_code",        limit: 50
    t.text     "slit_description"
    t.string   "coilno",           limit: 50
    t.float    "bldup",            limit: 53
    t.float    "qty",              limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "username",         limit: 50
    t.integer  "slitid",           limit: 8
    t.string   "pressno",          limit: 50
    t.integer  "ALLOCATED",                   default: 0
    t.string   "category",         limit: 50
  end

  create_table "Allocated_stock", primary_key: "ID", force: true do |t|
    t.string   "orderno",          limit: 50
    t.string   "Item_code",        limit: 50
    t.float    "QTY",              limit: 53
    t.text     "Item_description"
    t.integer  "status",                      default: 0,     null: false
    t.datetime "date"
    t.integer  "month"
    t.integer  "Year"
    t.string   "username",         limit: 50
    t.string   "category",         limit: 50
    t.boolean  "self",                        default: false
  end

  create_table "Allocation_Press", id: false, force: true do |t|
    t.string "Pressno", limit: 50
    t.float  "QTY",     limit: 53
    t.float  "Mins",    limit: 53
  end

  create_table "Alu_Master", force: true do |t|
    t.string "Item_code"
    t.string "item_description"
  end

  create_table "Alu_Masters", id: false, force: true do |t|
    t.integer "id",               limit: 8
    t.string  "Item_code"
    t.string  "item_description"
  end

  create_table "Alu_Openning", id: false, force: true do |t|
    t.string  "Item_code"
    t.float   "qty",       limit: 53
    t.integer "month"
    t.integer "year"
  end

  create_table "Alu_stock", id: false, force: true do |t|
    t.float "code",  limit: 53
    t.float "gauge", limit: 53
    t.float "qty",   limit: 53
  end

  create_table "Aluware_stock", id: false, force: true do |t|
    t.string "Code",         limit: 50
    t.float  "Openning_stk", limit: 53
    t.float  "Receipts",     limit: 53
    t.float  "Issues",       limit: 53
    t.float  "Stk_hand",     limit: 53
  end

  create_table "Balance_To_Pack", id: false, force: true do |t|
    t.string "Orderno",   limit: 50
    t.string "item_code", limit: 10
    t.float  "OrderQty",  limit: 53
    t.float  "Packed",    limit: 53
    t.float  "Balance",   limit: 53
  end

  create_table "Batch", force: true do |t|
    t.integer "batch", limit: 8
  end

  create_table "Batch_allocated_slit", id: false, force: true do |t|
    t.integer "ID",          limit: 8
    t.string  "pressno",     limit: 50
    t.string  "Batchno",     limit: 50
    t.float   "Circle_size", limit: 53
    t.float   "slit_code",   limit: 53
    t.float   "Slit_WT",     limit: 53
  end

  create_table "Batch_list", id: false, force: true do |t|
    t.string "item_code",        limit: 50
    t.text   "Item_description"
    t.float  "qty",              limit: 53
    t.string "Batchno",          limit: 50
    t.string "Shiftname",        limit: 50
    t.string "category",         limit: 50
  end

  create_table "Batches", id: false, force: true do |t|
    t.text     "batch"
    t.datetime "committ"
  end

  create_table "Batches_Allowed", primary_key: "Id", force: true do |t|
    t.integer "Batchnos", limit: 8
    t.boolean "uselimit"
  end

  create_table "Blanking_Plan", force: true do |t|
    t.string   "item_code",     limit: 50
    t.float    "qty",           limit: 53
    t.string   "Pressno",       limit: 50
    t.string   "batchno",       limit: 50
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.integer  "HasSlit",                  default: 0
    t.float    "mins",          limit: 53
    t.string   "Specification", limit: 50
    t.string   "Username",      limit: 50
    t.float    "Toplan",        limit: 53
  end

  create_table "Blanking_Recovery", id: false, force: true do |t|
    t.integer "Recovery", limit: 8
  end

  create_table "Blanking_allowed", id: false, force: true do |t|
    t.integer "id"
    t.float   "Blkper",   limit: 53
    t.boolean "Allow"
    t.string  "Customer", limit: 50
  end

  create_table "Blankingissues", force: true do |t|
    t.text     "Item_code"
    t.text     "Item_description"
    t.string   "orderno"
    t.float    "qty",              limit: 53
    t.float    "issued",           limit: 53, default: 0.0
    t.float    "remaining",        limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.integer  "status",                      default: 0
    t.string   "Next_process"
    t.string   "trayno"
    t.string   "username"
    t.string   "coilno",           limit: 50
    t.string   "Batchno",          limit: 50
    t.integer  "transfer",                    default: 0,   null: false
    t.integer  "blkid",            limit: 8
    t.float    "buildup",          limit: 53, default: 0.0
    t.float    "gauge",            limit: 53
    t.string   "Category",         limit: 50
  end

  create_table "Blankingissues1", primary_key: "Id", force: true do |t|
    t.string   "Item_code",        limit: 50
    t.text     "Item_description"
    t.string   "orderno",          limit: 50
    t.float    "qty",              limit: 53
    t.float    "issued",           limit: 53, default: 0.0
    t.float    "remaining",        limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.integer  "status",                      default: 0
    t.string   "Next_process",     limit: 50
    t.string   "Trayno",           limit: 10
    t.string   "username",         limit: 50
  end

  create_table "CC_scrap", id: false, force: true do |t|
    t.integer "no"
    t.string  "pressno", limit: 50
    t.float   "input",   limit: 53
    t.float   "output",  limit: 53
    t.float   "scrap",   limit: 53
  end

  create_table "Caster_closing_temps", id: false, force: true do |t|
    t.string "Name",    limit: 50
    t.float  "Opstk",   limit: 53
    t.float  "receipt", limit: 53
    t.float  "issue",   limit: 53
    t.float  "closing", limit: 53
  end

  create_table "Casting_plan", id: false, force: true do |t|
    t.string   "Temper",   limit: 50
    t.float    "QTY",      limit: 53
    t.integer  "Masterno", limit: 8
    t.datetime "Date"
  end

  create_table "Category_name", force: true do |t|
    t.string "name",        limit: 50
    t.string "Description", limit: 50
  end

  create_table "Circles", primary_key: "ID", force: true do |t|
    t.string "Item_description"
    t.string "item_code"
    t.string "Category"
    t.float  "WT",               limit: 53, default: 1.0
    t.string "DIVISION"
    t.string "UOM"
    t.string "Item_code1"
    t.string "categoryRoute"
  end

  create_table "Circles1", force: true do |t|
    t.text   "item_description"
    t.string "Item_code"
    t.string "Category"
  end

  create_table "Circles2", force: true do |t|
    t.string "item_description"
    t.string "Item_code"
    t.string "Category"
    t.float  "Item_wt",          limit: 53
    t.string "DIVISION",                    default: "KARM"
    t.string "UOM"
    t.string "Item_code1"
    t.string "categoryRoute",    limit: 50
  end

  create_table "Circles3", primary_key: "ID", force: true do |t|
    t.string "item_description"
    t.string "Item_code"
    t.string "Category"
    t.float  "Item_WT",          limit: 53
    t.string "Division",         limit: 50
    t.string "UOM",              limit: 10
    t.string "Item_code1",       limit: 50
  end

  create_table "Circles_missing", id: false, force: true do |t|
    t.integer "id",        limit: 8
    t.string  "Item_Code", limit: 50
  end

  create_table "Coil_allocation_Batch", force: true do |t|
    t.string "Item_code", limit: 50
    t.float  "Coil_QTY",  limit: 53
    t.string "Batchno",   limit: 50
    t.string "coil_no",   limit: 50
  end

  create_table "Coil_consumption", id: false, force: true do |t|
    t.float "size",  limit: 53
    t.float "qty",   limit: 53
    t.float "gauge", limit: 53
  end

  create_table "Coil_number", id: false, force: true do |t|
    t.integer "coilno", limit: 8
  end

  create_table "ColdMill_plan", id: false, force: true do |t|
    t.float    "Thickness", limit: 53
    t.string   "Temper",    limit: 50
    t.float    "Qty",       limit: 53
    t.integer  "Masterno",  limit: 8
    t.datetime "date"
  end

  create_table "Current_blanking", force: true do |t|
    t.string "item_code", limit: 50
    t.string "Batchno",   limit: 50
    t.string "username",  limit: 50
  end

  create_table "Current_order_down_loading", force: true do |t|
    t.string "username", limit: 50
  end

  create_table "Customer_listing", force: true do |t|
    t.string "Name"
    t.string "code"
    t.string "Package_type", limit: 50
  end

  create_table "Customers", primary_key: "ID", force: true do |t|
    t.string "Name", limit: 50
  end

  create_table "Customers_emailaddress", primary_key: "Id", force: true do |t|
    t.string "Customer",     limit: 50
    t.string "Emailaddress", limit: 150
  end

  create_table "Daystore_Openning_stock", id: false, force: true do |t|
    t.string   "ITEM_NAME"
    t.string   "RMCODE"
    t.float    "rmid",      limit: 53
    t.float    "QTY",       limit: 53
    t.integer  "MONTH"
    t.integer  "YEAR"
    t.float    "CENTREID",  limit: 53
    t.datetime "DATE"
    t.integer  "Center_id"
  end

  create_table "Daystore_Openning_stock1", id: false, force: true do |t|
    t.string  "item_name", limit: 50
    t.float   "qty",       limit: 53
    t.integer "month"
    t.integer "year"
  end

  create_table "Daystore_Openning_stock13", id: false, force: true do |t|
    t.string   "ITEM_NAME"
    t.string   "RMCODE"
    t.float    "QTY",       limit: 53
    t.float    "MONTH",     limit: 53
    t.float    "YEAR",      limit: 53
    t.float    "CENTREID",  limit: 53
    t.datetime "DATE"
  end

  create_table "Daystore_Openning_stock14", id: false, force: true do |t|
    t.string   "item_name"
    t.string   "rmcode"
    t.float    "qty",       limit: 53
    t.float    "month",     limit: 53
    t.float    "year",      limit: 53
    t.integer  "Centerid",  limit: 8
    t.datetime "date"
  end

  create_table "Dispatch", primary_key: "ID", force: true do |t|
    t.string   "Palletno", limit: 50
    t.string   "Invno"
    t.float    "qty",      limit: 53
    t.integer  "month"
    t.integer  "year"
    t.datetime "date"
    t.string   "Orderno"
  end

  create_table "Divisions", id: false, force: true do |t|
    t.string  "division_name", limit: 50
    t.decimal "divisionid",               precision: 18, scale: 0
  end

  create_table "ExportApplication", primary_key: "exportAppId", force: true do |t|
    t.string   "exporterName",       limit: 200
    t.string   "exporterAddress",    limit: 200
    t.string   "exporterCountry",    limit: 50
    t.string   "consigneeName",      limit: 200
    t.string   "consigneeAddress",   limit: 200
    t.string   "consigneeCountry",   limit: 200
    t.string   "tradingCountry1",    limit: 50
    t.string   "tradingCountry2",    limit: 50
    t.string   "originCountry",      limit: 50
    t.string   "destinationCountry", limit: 50
    t.string   "remarks",            limit: 500
    t.string   "itemnumber",         limit: 50
    t.string   "marks",              limit: 50
    t.string   "kindOfPackages",     limit: 50
    t.float    "grossWeight",        limit: 53
    t.string   "transParticulars",   limit: 50
    t.string   "descOfGoods",        limit: 500
    t.string   "invoices",           limit: 500
    t.string   "userName",           limit: 50
    t.datetime "dateOfEntry"
  end

  create_table "ExportComesa", force: true do |t|
    t.string   "exporter",           limit: 150
    t.string   "address",            limit: 150
    t.string   "consigneeName",      limit: 150
    t.string   "consigneeAdd",       limit: 150
    t.string   "goodsDesc",          limit: 1000
    t.string   "declarationRmk",     limit: 1000
    t.string   "custTarrifNo",       limit: 150
    t.string   "originCriterion",    limit: 150
    t.float    "grossWeight",        limit: 53
    t.string   "invoiceNo",          limit: 150
    t.string   "originCountry",      limit: 150
    t.string   "transportCriterion", limit: 150
    t.float    "noOfPackage",        limit: 53
    t.string   "kindOfPack",         limit: 150
    t.string   "userName",           limit: 150
    t.datetime "date"
    t.string   "refNo",              limit: 50
  end

  create_table "Export_Packing_list", force: true do |t|
    t.string "orderno",   limit: 50
    t.string "boxno",     limit: 50
    t.string "item_code", limit: 50
    t.float  "qty",       limit: 53
  end

  create_table "FGissues", primary_key: "ID", force: true do |t|
    t.string   "CentreID",         limit: 50
    t.string   "Item_code",        limit: 50
    t.text     "Item_description"
    t.datetime "Date",                                    null: false
    t.string   "Coil_Number",      limit: 50
    t.float    "Qty",              limit: 53
    t.string   "Month",            limit: 50
    t.integer  "Year"
    t.integer  "status",                      default: 0, null: false
  end

  create_table "FOUNDRY", force: true do |t|
    t.string   "ITEM",     limit: 50
    t.text     "CUSTOMER"
    t.float    "QTY",      limit: 53
    t.datetime "DATE"
    t.integer  "MONTH"
    t.integer  "YEAR"
    t.string   "username", limit: 50
  end

  create_table "Fglocations", id: false, force: true do |t|
    t.string  "Location",   limit: 50
    t.decimal "id",                    precision: 18, scale: 0
    t.integer "divisionid"
  end

  create_table "Fheader", id: false, force: true do |t|
    t.text "TIT"
  end

  create_table "Flat_mill_cross_rolling", id: false, force: true do |t|
    t.string   "Coilno",            limit: 50
    t.string   "Slit_code",         limit: 50
    t.float    "input",             limit: 53
    t.float    "Output",            limit: 53
    t.datetime "date"
    t.float    "Achieved_Diameter", limit: 53
    t.integer  "Passes"
  end

  create_table "Foundry_Packing", force: true do |t|
    t.string   "Item_code"
    t.string   "Item_description"
    t.datetime "date"
    t.string   "coil_number"
    t.float    "qty",              limit: 53
    t.string   "month"
    t.string   "year"
    t.string   "station"
    t.integer  "status",                      default: 0
    t.string   "Orderno"
    t.string   "username"
    t.string   "Shiftname"
    t.string   "orderwise"
    t.integer  "pallet",                      default: 0
    t.integer  "Transfer",                    default: 1
    t.string   "Batchno"
    t.string   "Category"
    t.string   "Customer"
    t.string   "item_code1"
  end

  create_table "Furnace_charging", force: true do |t|
    t.string   "item_name", limit: 50
    t.datetime "date"
    t.string   "username",  limit: 50
    t.float    "qty",       limit: 53
    t.integer  "month"
    t.integer  "Year"
    t.integer  "rmid",      limit: 8
    t.integer  "status",               default: 0
    t.integer  "furid",     limit: 8
    t.float    "Box",       limit: 53, default: 0.0
    t.float    "Netwt",     limit: 53, default: 0.0
    t.string   "Shiftname", limit: 50
  end

  create_table "Gauge_balances", id: false, force: true do |t|
    t.string "item_code", limit: 50
    t.float  "qty",       limit: 53
    t.string "orderno",   limit: 50
  end

  create_table "INVOICES_SAP", primary_key: "Id", force: true do |t|
    t.string   "orderno",          limit: 50
    t.text     "Customer"
    t.string   "Item_code",        limit: 50
    t.text     "Item_description"
    t.float    "qty",              limit: 53
    t.string   "palletno",         limit: 50
    t.text     "invoiceno",                                 null: false
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "Category",         limit: 50
    t.string   "INVNOLINE",        limit: 50,               null: false
    t.string   "LINENUM",          limit: 50,               null: false
    t.string   "NS_SoNo",          limit: 50
    t.integer  "pick",                        default: 0
    t.float    "Cut_len",          limit: 53, default: 0.0
    t.float    "Qty_pcs",          limit: 53, default: 0.0
  end

  create_table "Internal_Customer", primary_key: "ID", force: true do |t|
    t.string   "Batchno",    limit: 50
    t.string   "Item_code",  limit: 50
    t.float    "Qty",        limit: 53
    t.integer  "status",                default: 0, null: false
    t.datetime "Close_date"
    t.integer  "year"
    t.integer  "Pick",                  default: 0
    t.integer  "Approved",              default: 0
    t.string   "Category",   limit: 50
  end

  create_table "Invoices", primary_key: "Id", force: true do |t|
    t.string   "orderno",          limit: 50
    t.text     "Customer"
    t.string   "Item_code",        limit: 50
    t.text     "Item_description"
    t.float    "qty",              limit: 53
    t.string   "palletno",         limit: 50
    t.string   "invoiceno",        limit: 50,               null: false
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "Category",         limit: 50
    t.integer  "LINENUM"
    t.float    "CUT_LEN",          limit: 53, default: 0.0
    t.float    "QTY_PCS",          limit: 53, default: 0.0
    t.integer  "STATUS",                      default: 0
  end

  create_table "Issued_Rolled_coils", force: true do |t|
    t.string   "coilno",      limit: 50
    t.float    "coilwidth",   limit: 53
    t.float    "coilbuildup", limit: 53
    t.float    "gauge",       limit: 53
    t.float    "qty",         limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "username",    limit: 50
    t.integer  "status",                 default: 0
    t.string   "category",    limit: 50
    t.string   "Nextprocess", limit: 50
    t.string   "orderno",     limit: 50
    t.integer  "Batchno",     limit: 8
  end

  create_table "Issued_squares", force: true do |t|
    t.string   "coilno",           limit: 50
    t.string   "item_code",        limit: 50
    t.text     "item_description"
    t.float    "qty",              limit: 53
    t.float    "remaining",        limit: 53
    t.float    "issued",           limit: 53, default: 0.0
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "Machine",          limit: 50
    t.float    "recovery",         limit: 53
    t.integer  "status",                      default: 0
  end

  create_table "Item_routes", force: true do |t|
    t.string "R1",            limit: 10
    t.string "R2",            limit: 10
    t.string "R3",            limit: 10
    t.string "R4",            limit: 10
    t.string "R5",            limit: 10
    t.string "R6",            limit: 10
    t.string "R7",            limit: 10
    t.string "Category_type", limit: 50
  end

  create_table "JobCard_Input", id: false, force: true do |t|
    t.datetime "Date"
    t.string   "Coilno",     limit: 50
    t.string   "Input_Item", limit: 50
    t.float    "qty",        limit: 53
  end

  create_table "Jobcard_Header", id: false, force: true do |t|
    t.integer  "Report_section"
    t.string   "Customer",       limit: 50
    t.string   "Orderno",        limit: 50
    t.datetime "Date"
  end

  create_table "Loading_Pallets", force: true do |t|
    t.float    "NETWT",        limit: 53
    t.float    "grosswt",      limit: 53
    t.string   "Palletno",     limit: 50
    t.float    "palletWT",     limit: 53
    t.float    "DIFFWT",       limit: 53
    t.string   "Invoiceno",    limit: 50
    t.string   "Security_man", limit: 50
    t.string   "Drivername",   limit: 50
    t.string   "Truck_name",   limit: 50
    t.string   "Transporter",  limit: 50
    t.datetime "date"
    t.string   "username",     limit: 50
    t.string   "Customer",     limit: 50
    t.integer  "Year"
  end

  create_table "MMSLoading", primary_key: "WeightID", force: true do |t|
    t.string   "PalletNo",     limit: 50
    t.string   "InvoiceNo",    limit: 50
    t.string   "CustomerName", limit: 50
    t.string   "TruckNo",      limit: 50
    t.string   "DriverName",   limit: 50
    t.string   "Destination",  limit: 50
    t.string   "Security",     limit: 50
    t.string   "UserName",     limit: 50
    t.date     "DateIn"
    t.datetime "TimeIn"
    t.float    "TareWeight",   limit: 53
    t.date     "DateOut"
    t.datetime "TimeOut"
    t.float    "GrossWeight",  limit: 53
    t.float    "Netweight",    limit: 53
    t.float    "DiffWeight",   limit: 53
    t.string   "Transporter",  limit: 50
    t.datetime "Date"
    t.string   "Year",         limit: 5
    t.string   "Status",       limit: 50
    t.string   "Category",     limit: 50
  end

  create_table "Macro_Summary", primary_key: "ID", force: true do |t|
    t.integer  "batchno",   limit: 8
    t.string   "Item_code", limit: 50
    t.float    "order_qty", limit: 53
    t.float    "plan_qty",  limit: 53
    t.float    "qty",       limit: 53
    t.float    "slit_wt",   limit: 53
    t.float    "buildup",   limit: 53
    t.float    "coil_part", limit: 53
    t.float    "slit_size", limit: 53
    t.text     "memo",                 null: false
    t.string   "coilno",    limit: 50
    t.string   "username",  limit: 50
    t.datetime "Date"
    t.float    "Sltstk",    limit: 53
    t.float    "CRM",       limit: 53
    t.float    "CC",        limit: 53
    t.string   "category",  limit: 50
    t.text     "Remarks"
  end

  create_table "Macro_plan", id: false, force: true do |t|
    t.string "item_code",    limit: 50
    t.float  "qty",          limit: 53
    t.float  "Hard",         limit: 53
    t.float  "slits",        limit: 53
    t.float  "Reroll",       limit: 53
    t.float  "Rolled_coils", limit: 53
    t.float  "Coil",         limit: 53
    t.float  "To_plan",      limit: 53
    t.float  "Casting",      limit: 53
  end

  create_table "Macroslist", id: false, force: true do |t|
    t.string "item_code", limit: 50
    t.float  "order_qty", limit: 53
    t.float  "qty_Plan",  limit: 53
    t.float  "qty",       limit: 53
    t.float  "slitwt",    limit: 53
    t.float  "bldup",     limit: 53
    t.float  "slitpart",  limit: 53
    t.float  "slitwidth", limit: 53
    t.text   "coilno"
    t.float  "Sltstk",    limit: 53
    t.float  "CRM",       limit: 53
    t.float  "CC",        limit: 53
  end

  create_table "Master_plan_orders", id: false, force: true do |t|
    t.string   "Order_no",  limit: 50
    t.float    "Diameter",  limit: 53
    t.float    "Thickness", limit: 53
    t.string   "Temper",    limit: 50
    t.float    "Qty",       limit: 53
    t.integer  "Masterno",  limit: 8
    t.datetime "date"
  end

  create_table "Meltloss_Figures", force: true do |t|
    t.float    "OpenningQty",    limit: 53
    t.float    "MF",             limit: 53
    t.float    "HF",             limit: 53
    t.float    "CoilonRewinder", limit: 53
    t.float    "ClosingQTY",     limit: 53
    t.boolean  "Closed",                    default: false
    t.datetime "Date"
    t.string   "Caster",         limit: 50
    t.float    "Drains",         limit: 53
    t.integer  "Month"
    t.integer  "Year"
  end

  create_table "Metal_Store_openning", id: false, force: true do |t|
    t.string   "ITEM_NAME"
    t.string   "RMCODE"
    t.float    "rmid",      limit: 53
    t.float    "QTY",       limit: 53
    t.float    "MONTH",     limit: 53
    t.float    "YEAR",      limit: 53
    t.datetime "DATE"
  end

  create_table "Metal_Store_openning1", id: false, force: true do |t|
    t.string  "item_name", limit: 50
    t.float   "qty",       limit: 53
    t.integer "month"
    t.integer "year"
  end

  create_table "Metal_Store_openning13", id: false, force: true do |t|
    t.string   "rmDescription"
    t.string   "RMCODE"
    t.float    "QTY",           limit: 53
    t.float    "MONTH",         limit: 53
    t.float    "YEAR",          limit: 53
    t.datetime "DATE"
  end

  create_table "Metal_Store_openning15", id: false, force: true do |t|
    t.string   "item_name"
    t.string   "rmcode"
    t.float    "qty",       limit: 53
    t.float    "month",     limit: 53
    t.float    "year",      limit: 53
    t.integer  "Centerid",  limit: 8
    t.datetime "date"
  end

  create_table "Metal_issued", force: true do |t|
    t.string   "item_name", limit: 50
    t.datetime "date"
    t.string   "username",  limit: 50
    t.float    "qty",       limit: 53
    t.string   "remarks",   limit: 50
    t.integer  "Month"
    t.integer  "Year"
  end

  create_table "Metal_receipts", force: true do |t|
    t.string   "Item_Name", limit: 50
    t.string   "Supplier",  limit: 50
    t.string   "Dno",       limit: 50
    t.datetime "date"
    t.string   "Username",  limit: 50
    t.float    "QTY",       limit: 53
    t.string   "Remarks",   limit: 50
    t.integer  "month"
    t.integer  "year"
    t.integer  "rmid",      limit: 8
    t.float    "Tarewt",    limit: 53
    t.float    "Netwt",     limit: 53
    t.integer  "status",               default: 0
    t.integer  "storeId",   limit: 8
  end

  create_table "Metal_requisition", id: false, force: true do |t|
    t.integer  "id",               limit: 8
    t.string   "item_description", limit: 50
    t.float    "Required_qty",     limit: 53
    t.datetime "date"
    t.integer  "Doc_no",           limit: 8
  end

  create_table "MotherCodeBOM", primary_key: "ID", force: true do |t|
    t.float "motherCodeID", limit: 53
    t.float "childQtty",    limit: 53
    t.float "childID",      limit: 53
  end

  create_table "MotherCodeBOM3", id: false, force: true do |t|
    t.float "motherCodeID", limit: 53
    t.float "childQtty",    limit: 53
    t.float "childID",      limit: 53
  end

  create_table "Mother_code_Reorder", primary_key: "Id", force: true do |t|
    t.string   "item_code",        limit: 50
    t.text     "item_description"
    t.float    "qty",              limit: 53
    t.datetime "date"
    t.boolean  "approved",                    default: false
    t.string   "username",         limit: 50
    t.float    "CNT",              limit: 53
  end

  create_table "Mother_codes", primary_key: "ID", force: true do |t|
    t.string "AKW_FG_CODE",      limit: 50
    t.string "AKWFGDESCRIPTION"
    t.float  "CTNS_PER_PALLET",  limit: 53
    t.string "UOM"
    t.float  "SAFETY",           limit: 53
    t.string "PRODUCT_CLASS",    limit: 10
  end

  create_table "Mother_codes1", primary_key: "ID", force: true do |t|
    t.string "AKW_FG_CODE"
    t.string "AKWFGDESCRIPTION"
    t.string "UOM"
  end

  create_table "Mother_codes3", primary_key: "ID", force: true do |t|
    t.string "AKW_FG_CODE"
    t.string "AKWFGDESCRIPTION"
    t.float  "CTNS_PER_PALLET",  limit: 53
    t.string "UOM"
  end

  create_table "Netsuite", id: false, force: true do |t|
    t.string "Username", limit: 50
    t.string "password", limit: 50
  end

  create_table "Openningstock1", force: true do |t|
    t.string  "item_code"
    t.string  "Item_description"
    t.float   "qty",              limit: 53
    t.integer "month"
    t.integer "year"
    t.string  "Category"
  end

  create_table "Order_pulling", id: false, force: true do |t|
    t.string "code",         limit: 50
    t.float  "Order_Qty",    limit: 53
    t.float  "Hard",         limit: 53
    t.float  "Soft",         limit: 53
    t.float  "Unpalletized", limit: 53
    t.float  "Pallets",      limit: 53
    t.string "Orderno",      limit: 50
    t.float  "Bal",          limit: 53
    t.float  "Disp",         limit: 53
  end

  create_table "Orders", force: true do |t|
    t.string   "orderno"
    t.string   "Customer"
    t.string   "Item_code"
    t.text     "item_description"
    t.float    "qty",              limit: 53
    t.datetime "DATE"
    t.string   "memo"
  end

  create_table "Orders1", force: true do |t|
    t.string   "orderno",          limit: 50
    t.text     "customer"
    t.string   "item_code",        limit: 50
    t.text     "item_description"
    t.float    "qty",              limit: 53
    t.datetime "date"
    t.string   "memo",             limit: 10, default: "M"
  end

  create_table "Orders2", id: false, force: true do |t|
    t.integer  "ID",               limit: 8
    t.string   "Orderno"
    t.text     "customer"
    t.string   "Item_code"
    t.text     "Item_Description"
    t.float    "Qty",              limit: 53
    t.datetime "DATE"
    t.string   "Memo",                        default: "M"
  end

  create_table "Orders3", force: true do |t|
    t.string   "orderno"
    t.text     "Customer"
    t.string   "Item_code"
    t.text     "item_description"
    t.float    "qty",              limit: 53
    t.datetime "DATE"
    t.text     "memo",                        default: "m"
  end

  create_table "Packing_list", primary_key: "Id", force: true do |t|
    t.string   "Orderno", limit: 50
    t.datetime "date"
    t.integer  "status",             default: 1
    t.float    "Qty",     limit: 53
    t.integer  "month"
    t.integer  "year"
  end

  create_table "Packing_summary", id: false, force: true do |t|
    t.string "item_code",        limit: 50
    t.text   "item_description"
    t.string "Orderno",          limit: 50
    t.float  "qty",              limit: 53
  end

  create_table "Pallet_Master", force: true do |t|
    t.float  "max_size",      limit: 53
    t.string "Pallet_type"
    t.string "item_category", limit: 50
    t.string "Top_pallets"
    t.float  "MAX_QTY",       limit: 53
  end

  create_table "Pallet_Master1", primary_key: "ID", force: true do |t|
    t.string "Pallet_type", limit: 50
    t.float  "Min_size",    limit: 53
    t.float  "Max_size",    limit: 53
  end

  create_table "Pallet_numbering", force: true do |t|
    t.integer "Nextnumber", limit: 8
    t.string  "Customer",   limit: 50
    t.integer "year"
    t.string  "Category",   limit: 50
  end

  create_table "Pallet_requirement", primary_key: "ID", force: true do |t|
    t.string  "ORDERNO", limit: 50
    t.float   "QTY",     limit: 53
    t.string  "BATCHNO", limit: 50
    t.integer "Status"
  end

  create_table "Pallet_stock", id: false, force: true do |t|
    t.string "item_code",        limit: 50
    t.text   "item_description"
    t.string "orderno",          limit: 50
    t.string "palletno",         limit: 50
    t.string "customer",         limit: 50
    t.float  "qty",              limit: 53
  end

  create_table "Pallets", force: true do |t|
    t.string   "Item_code"
    t.text     "Item_description"
    t.string   "Palletno"
    t.float    "QTY",              limit: 53
    t.datetime "Date"
    t.integer  "MONTH"
    t.integer  "YEAR"
    t.text     "Customer"
    t.string   "Orderno"
    t.integer  "status",                      default: 0
    t.integer  "dispatch",                    default: 0
    t.string   "Category"
    t.integer  "packid",           limit: 8
    t.integer  "locid",            limit: 8
    t.string   "username",         limit: 50
    t.integer  "Transfer",                    default: 1
    t.integer  "salid",            limit: 8
    t.string   "item_type",        limit: 50
    t.boolean  "Isready",                     default: false
  end

  create_table "Pallets18", force: true do |t|
    t.string   "Item_code"
    t.text     "Item_description"
    t.string   "Palletno",         limit: 50
    t.float    "QTY",              limit: 53
    t.datetime "Date"
    t.integer  "MONTH"
    t.integer  "YEAR"
    t.string   "Customer"
    t.string   "Orderno"
    t.integer  "status",                      default: 0
    t.integer  "dispatch",                    default: 0
    t.string   "Category",         limit: 50
  end

  create_table "Pardini_Items", id: false, force: true do |t|
    t.string "Item_code"
    t.string "Item_description"
    t.string "trade_code"
  end

  create_table "Pardini_orders", force: true do |t|
    t.string   "Orderno",   limit: 50
    t.string   "Item_code", limit: 50
    t.float    "qty",       limit: 53
    t.datetime "date"
  end

  create_table "Pardini_stocks", force: true do |t|
    t.string  "Item_code",  limit: 50
    t.float   "qty",        limit: 53
    t.integer "locationid", limit: 8
    t.text    "location"
  end

  create_table "Press_Allocation", force: true do |t|
    t.integer "Press_id",   limit: 8
    t.float   "size",       limit: 53
    t.float   "Production", limit: 53
    t.float   "gauge",      limit: 53
    t.float   "mins",       limit: 53
  end

  create_table "Press_Allocation1", force: true do |t|
    t.integer "Press_Id",   limit: 8
    t.float   "size",       limit: 53
    t.float   "Production", limit: 53
    t.float   "Gauge",      limit: 53
    t.decimal "mins",                  precision: 18, scale: 0
  end

  create_table "Press_master", primary_key: "ID", force: true do |t|
    t.string  "Pressno", limit: 50
    t.integer "Status",             default: 0
  end

  create_table "Presss_size_allocation", primary_key: "ID", force: true do |t|
    t.string "Pressno",     limit: 50
    t.float  "slit_code",   limit: 53
    t.float  "circle_size", limit: 53
  end

  create_table "Presswise", id: false, force: true do |t|
    t.string "batchno", limit: 50
    t.string "code",    limit: 50
    t.string "coilno",  limit: 50
    t.float  "Buildup", limit: 53
    t.float  "Qty",     limit: 53
    t.string "pressno", limit: 50
  end

  create_table "Process_Name", primary_key: "processid", force: true do |t|
    t.string  "name",     limit: 30
    t.integer "status",              default: 0
    t.string  "leadTime", limit: 50
    t.string  "userName", limit: 50
  end

  create_table "Process_Route", force: true do |t|
    t.integer "Productid", limit: 8
    t.integer "processid", limit: 8
    t.integer "processno", limit: 8
  end

  create_table "Product_Category", id: false, force: true do |t|
    t.integer "id",   limit: 8
    t.string  "name", limit: 50
  end

  create_table "Proofing_transactions", force: true do |t|
    t.integer "Processid",    limit: 8
    t.string  "Coilno",       limit: 50
    t.float   "qty_Input",    limit: 53
    t.float   "Qty_0up",      limit: 53
    t.float   "Scrap",        limit: 53
    t.float   "Reason_scrap", limit: 53
  end

  create_table "Rec_numbering", id: false, force: true do |t|
    t.integer "recno", limit: 8
  end

  create_table "Returns", primary_key: "ID", force: true do |t|
    t.string   "Item_code",        limit: 50
    t.text     "Item_description"
    t.float    "qty",              limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "customer",         limit: 50
    t.string   "category",         limit: 50
  end

  create_table "Rolled_Coils_Allocated_To_Batch", id: false, force: true do |t|
    t.string "item_code", limit: 50
    t.float  "coil_qty",  limit: 53
    t.string "batchno",   limit: 50
    t.string "coil_no",   limit: 50
  end

  create_table "Rolled_coils", force: true do |t|
    t.string   "coilno",      limit: 50
    t.float    "coilwidth",   limit: 53
    t.float    "coilbuildup", limit: 53
    t.float    "gauge",       limit: 53
    t.float    "qty",         limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "username",    limit: 50
    t.integer  "coilid",      limit: 8
    t.integer  "Slits",                  default: 0
    t.integer  "Semi_rolled",            default: 0
    t.integer  "slitter",                default: 0
    t.float    "Remaining",   limit: 53, default: 0.0
    t.float    "Issued",      limit: 53, default: 0.0
    t.float    "Newbuildup",  limit: 53, default: 0.0
    t.integer  "Semi",                   default: 0
    t.integer  "Batchno",     limit: 8
  end

  create_table "Roofing_packing", primary_key: "ID", force: true do |t|
    t.string   "Item_code"
    t.string   "Item_description"
    t.datetime "date"
    t.string   "coil_number",      limit: 50
    t.float    "qty",              limit: 53
    t.integer  "month"
    t.integer  "year"
    t.string   "station"
    t.integer  "status",                      default: 0
    t.string   "Orderno"
    t.string   "username"
    t.string   "Shiftname"
    t.integer  "orderwise"
    t.integer  "pallet"
    t.integer  "Transfer",                    default: 0
    t.string   "Batchno",          limit: 50
    t.string   "Category",         limit: 50
    t.text     "Customer"
    t.float    "Cut_len",          limit: 53
    t.float    "qty_pcs",          limit: 53
    t.integer  "coilid",           limit: 8
  end

  create_table "SO_Batches", force: true do |t|
    t.string  "orderno"
    t.integer "Batchno",  limit: 8
    t.string  "Inter_So"
  end

  create_table "SO_DEMAND_SAP", primary_key: "Id", force: true do |t|
    t.string   "so_no",            limit: 50
    t.string   "item_code",        limit: 50
    t.text     "item_description"
    t.datetime "date_required"
    t.float    "qty_required",     limit: 53
    t.float    "qty_issued",       limit: 53
    t.float    "qty_remaining",    limit: 53
    t.text     "customer"
    t.text     "memo"
    t.integer  "Status",                                                             null: false
    t.datetime "committ_date"
    t.string   "category",         limit: 50
    t.datetime "Required_date"
    t.string   "PO_No",            limit: 50
    t.datetime "Close_date"
    t.text     "Remarks"
    t.datetime "Requested_Date"
    t.string   "Interdivision",    limit: 50
    t.integer  "pick",                                                 default: 0
    t.string   "item_code1",       limit: 50
    t.decimal  "LINENUM",                     precision: 18, scale: 0
    t.string   "sonoline",         limit: 50,                                        null: false
    t.float    "Suffuria_qty",     limit: 53
    t.float    "Cut_len",          limit: 53,                          default: 0.0
    t.float    "qty_pcs",          limit: 53,                          default: 0.0
  end

  create_table "SO_Demand1", primary_key: "ID", force: true do |t|
    t.string   "SO_No",            limit: 50
    t.text     "Item_code"
    t.text     "item_description"
    t.datetime "Date_Required"
    t.float    "Qty_Required",     limit: 53
    t.float    "Qty_Issued",       limit: 53
    t.float    "Qty_Remaining",    limit: 53
    t.text     "Customer"
    t.text     "Memo"
  end

  create_table "SO_Demand_TEMP", primary_key: "ID", force: true do |t|
    t.string   "SO_No",            limit: 50
    t.text     "Item_code"
    t.text     "item_description"
    t.datetime "Date_Required"
    t.float    "Qty_Required",     limit: 53
    t.float    "Qty_Issued",       limit: 53
    t.float    "Qty_Remaining",    limit: 53
    t.text     "Customer"
    t.text     "Memo"
  end

  create_table "SO_Demand_TEMPXX", primary_key: "ID", force: true do |t|
    t.string   "SO_No",            limit: 50
    t.text     "Item_code"
    t.text     "item_description"
    t.datetime "Date_Required"
    t.float    "Qty_Required",     limit: 53
    t.float    "Qty_Issued",       limit: 53
    t.float    "Qty_Remaining",    limit: 53
    t.text     "Customer"
    t.text     "Memo"
  end

  create_table "Salrecpt", id: false, force: true do |t|
    t.string "Item_code",        limit: 50
    t.text   "Item_description"
    t.string "Orderno",          limit: 50
    t.float  "qty",              limit: 53
  end

  create_table "Salvage", primary_key: "ID", force: true do |t|
    t.string   "Item_Code"
    t.string   "Item_description"
    t.string   "coilnumber"
    t.datetime "date"
    t.float    "qty",              limit: 53
    t.float    "Scrap",            limit: 53
    t.integer  "month"
    t.integer  "year"
    t.integer  "Wipid",            limit: 8
    t.float    "Input_qty",        limit: 53
    t.integer  "Status",                      default: 0
    t.string   "orderno",          limit: 50
    t.integer  "Pallet",                      default: 0
    t.string   "category",         limit: 50
  end

  create_table "Salvages", primary_key: "ID", force: true do |t|
    t.string   "Item_Code",        limit: 50
    t.text     "Item_description"
    t.string   "coilnumber",       limit: 50
    t.datetime "date"
    t.float    "qty",              limit: 53
    t.float    "Scrap",            limit: 53
    t.decimal  "month",                       precision: 18, scale: 0
    t.integer  "year"
    t.integer  "Wipid",            limit: 8
    t.float    "Input_qty",        limit: 53
    t.integer  "Status",                                               default: 0, null: false
  end

  create_table "Shift_name", primary_key: "Id", force: true do |t|
    t.string  "Shiftname",   limit: 50
    t.integer "Shift_hours"
  end

  create_table "Shiftlist", id: false, force: true do |t|
    t.string "item_Code",        limit: 50
    t.text   "Item_Description"
    t.string "Orderno",          limit: 50
    t.float  "QTY",              limit: 53
    t.string "Username",         limit: 50
  end

  create_table "Slit_stock", force: true do |t|
    t.integer  "lotno",            limit: 8
    t.string   "Slit_code"
    t.string   "slit_description"
    t.float    "Size",             limit: 53
    t.float    "bldup",            limit: 53
    t.float    "bldup_remains",    limit: 53
    t.float    "qty",              limit: 53
    t.float    "qty_Remaining",    limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "COILNO"
    t.integer  "status",                      default: 0,         null: false
    t.string   "username",         limit: 50
    t.float    "Thickness",        limit: 53
    t.integer  "Returns",                     default: 0
    t.string   "Location",                    default: "Returns"
    t.integer  "Allocated",                   default: 0
    t.integer  "Batchno",          limit: 8
    t.string   "Category"
  end

  create_table "Slit_stock1", primary_key: "Id", force: true do |t|
    t.integer  "Lotno",            limit: 8,  default: 0
    t.string   "slit_code"
    t.string   "slit_description"
    t.float    "Size",             limit: 53
    t.float    "BldUp",            limit: 53
    t.float    "bldup_remains",    limit: 53
    t.float    "Qty",              limit: 53
    t.float    "qty_Remaining",    limit: 53
    t.datetime "date"
    t.float    "month",            limit: 53
    t.float    "year",             limit: 53
    t.string   "CoilNo"
    t.integer  "status",                      default: 0
    t.string   "username"
    t.float    "Thickness",        limit: 53
    t.integer  "Returns",                     default: 0
    t.string   "Location",                    default: "Returns"
    t.integer  "Allocated",                   default: 0
    t.integer  "Batchno",          limit: 8
    t.string   "Category",         limit: 50
    t.integer  "Slitterid",        limit: 8
    t.float    "Slit_input",       limit: 53
  end

  create_table "Slitissues", force: true do |t|
    t.decimal  "Lotno",                        precision: 18, scale: 0
    t.string   "slit_code",        limit: 50
    t.text     "slit_description"
    t.float    "Size",             limit: 53
    t.float    "bldup",            limit: 53
    t.float    "qty",              limit: 53
    t.float    "remaining",        limit: 53
    t.float    "output",           limit: 53,                           default: 0.0
    t.float    "scrap",            limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "orderno",          limit: 50
    t.integer  "status",                                                default: 0,   null: false
    t.string   "recovery",         limit: 50
    t.string   "pressno",          limit: 50
    t.string   "coilno",           limit: 50
    t.string   "Location",         limit: 50
    t.string   "category",         limit: 50
    t.integer  "Reroll",                                                default: 0
    t.string   "New_Itemcode",     limit: 50
    t.integer  "statusRoofing",                                         default: 0
    t.string   "Reasons",          limit: 250
  end

  create_table "Slitreport", id: false, force: true do |t|
    t.float  "Dia",        limit: 53
    t.float  "thickness",  limit: 53
    t.float  "qty",        limit: 53
    t.float  "slitwidth",  limit: 53
    t.float  "slitweight", limit: 53
    t.float  "bldup",      limit: 53
    t.float  "slitlen",    limit: 53
    t.string "orderno",    limit: 50
    t.float  "Bldpart",    limit: 53
  end

  create_table "Slits", force: true do |t|
    t.float    "dia",        limit: 53
    t.float    "thickness",  limit: 53
    t.float    "qty",        limit: 53
    t.float    "slitwidth",  limit: 53
    t.float    "Slitweight", limit: 53
    t.float    "BLDUP",      limit: 53
    t.float    "BLDUPBAL",   limit: 53
    t.float    "Slitlen",    limit: 53,                                      null: false
    t.string   "orderno",    limit: 50
    t.decimal  "batchno",               precision: 18, scale: 0
    t.datetime "date",                                                       null: false
    t.integer  "month"
    t.integer  "year"
    t.integer  "status",                                         default: 0
    t.string   "Coilno",     limit: 50
  end

  create_table "Slits_mass", id: false, force: true do |t|
    t.string "coilno",    limit: 50
    t.float  "gauge",     limit: 53
    t.float  "qty",       limit: 53
    t.string "batchno",   limit: 50
    t.string "slit_code", limit: 50
    t.float  "BATCH_QTY", limit: 53
    t.string "ITEM_CODE", limit: 50
  end

  create_table "Slitter_plan", id: false, force: true do |t|
    t.integer  "Masterno",  limit: 8
    t.float    "Slit_size", limit: 53
    t.float    "Thickness", limit: 53
    t.string   "temper",    limit: 50
    t.float    "qty",       limit: 53
    t.datetime "date"
  end

  create_table "So_demand", force: true do |t|
    t.string   "so_no",            limit: 50
    t.string   "item_code",        limit: 50
    t.text     "item_description"
    t.datetime "date_required"
    t.float    "qty_required",     limit: 53
    t.float    "qty_issued",       limit: 53, default: 0.0
    t.float    "qty_remaining",    limit: 53
    t.text     "customer"
    t.text     "memo",                        default: "m"
    t.integer  "Status",                      default: 0,     null: false
    t.datetime "committ_date"
    t.string   "category",         limit: 50
    t.datetime "Required_date"
    t.string   "PO_No",            limit: 50
    t.datetime "Close_date"
    t.text     "Remarks"
    t.datetime "Requested_Date"
    t.string   "Interdivision",    limit: 50
    t.integer  "pick",                        default: 0
    t.string   "item_code1",       limit: 50
    t.float    "Cut_len",          limit: 53, default: 0.0
    t.float    "Qty_pcs",          limit: 53, default: 0.0
    t.string   "sonoline",         limit: 50
    t.integer  "motherid",         limit: 8,  default: 137
    t.text     "Reason"
    t.boolean  "Confirmed",                   default: false
    t.boolean  "Email",                       default: false
  end

  create_table "Squares_stocks", force: true do |t|
    t.string   "item_code",        limit: 50
    t.text     "item_description"
    t.string   "coilno",           limit: 50
    t.float    "qty",              limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.integer  "status",                      default: 0
    t.integer  "In_stock",                    default: 0
    t.string   "username",         limit: 50
    t.string   "shiftname",        limit: 50
    t.float    "remaining",        limit: 53
    t.float    "issued",           limit: 53, default: 0.0
    t.string   "Batchno",          limit: 50
    t.string   "Category",         limit: 50
  end

  create_table "Stage_transactions", force: true do |t|
    t.integer "Batchno",       limit: 8
    t.string  "Coilnumber",    limit: 50
    t.integer "productid",     limit: 8
    t.integer "Current_stage", limit: 8
    t.integer "Next_stage",    limit: 8
    t.float   "Input",         limit: 53
    t.float   "Output",        limit: 53, default: 0.0
    t.float   "Scrap",         limit: 53
    t.boolean "status"
  end

  create_table "Stock_at_hand", id: false, force: true do |t|
    t.string "Item_code",        limit: 50
    t.text   "Item_description"
    t.float  "Openstk",          limit: 53
    t.float  "Production",       limit: 53
    t.float  "Salvage_rec",      limit: 53
    t.float  "Return_qty",       limit: 53
    t.float  "Dispatch",         limit: 53
    t.float  "Rejection",        limit: 53
    t.float  "Salvage",          limit: 53
    t.float  "Stkhand",          limit: 53
  end

  create_table "Stocks", primary_key: "ID", force: true do |t|
    t.string "ITEM_code"
    t.text   "ITEM_DESCRIPTION"
    t.float  "Total",            limit: 53
  end

  create_table "Stores", primary_key: "ID", force: true do |t|
    t.string "StoreName", limit: 50
  end

  create_table "Suffuria_BOM", primary_key: "ID", force: true do |t|
    t.integer "Mother_ID",          limit: 8,  null: false
    t.string  "CIRCLE_USED"
    t.string  "CIRCLE_DESCRIPTION"
    t.string  "UOM"
    t.float   "Quantity_PER_CTN",   limit: 53
  end

  create_table "Timer_setting", id: false, force: true do |t|
    t.string  "machinecode", limit: 50
    t.integer "Interval_id"
  end

  create_table "TkwWip", force: true do |t|
    t.string "Item_code"
    t.text   "Description"
    t.float  "Qty",         limit: 53
  end

  create_table "Tkw_Wip", force: true do |t|
    t.string   "TRANID"
    t.datetime "TRANDATE"
    t.string   "Item_code"
    t.text     "DISPLAYNAME"
    t.float    "QTY",         limit: 53
    t.string   "Category"
    t.integer  "IN_Stock",               default: 0
  end

  create_table "Tkw_Wip1", force: true do |t|
    t.string   "item_code",         limit: 50
    t.text     "Item_description"
    t.datetime "Tran_date"
    t.float    "qty",               limit: 53
    t.string   "Location",          limit: 50
    t.integer  "Returned_to_stock",            default: 0
  end

  create_table "Tkw_bom", force: true do |t|
    t.string "Item_code"
    t.string "Item_Description"
    t.string "UoM"
    t.float  "Item_WT",          limit: 53
    t.string "Componet_code"
  end

  create_table "Tkw_bom1", force: true do |t|
    t.string "Item_code"
    t.string "Componet_code"
    t.float  "Item_Wt",       limit: 53
  end

  create_table "Tkw_bom2", force: true do |t|
    t.string "item_code"
    t.string "Componet_code"
    t.float  "Item_WT",       limit: 53
  end

  create_table "Tkwcolours", primary_key: "ID", force: true do |t|
    t.string "colourName", limit: 50
    t.string "colourCode", limit: 50
  end

  create_table "Trays", primary_key: "ID", force: true do |t|
    t.string "trayno", limit: 50
  end

  create_table "Unallocated", force: true do |t|
    t.string   "Item_CODE"
    t.text     "ITEM_description"
    t.float    "QTY",              limit: 53
    t.datetime "DATE"
    t.integer  "MONTH"
    t.integer  "YEAR"
    t.integer  "STATUS",                      default: 0
    t.string   "ORDERNO"
    t.string   "USERNAME"
    t.integer  "referenceid",      limit: 8
    t.string   "category",         limit: 50
  end

  create_table "Undispatched", id: false, force: true do |t|
    t.string "Item_description", limit: 1000
    t.float  "order_qty",        limit: 53
    t.float  "Pallet_Qty",       limit: 53
    t.float  "Bal_qty",          limit: 53
    t.string "Palletno",         limit: 50
    t.string "Orderno",          limit: 50
  end

  create_table "Users", primary_key: "Userid", force: true do |t|
    t.text    "Names"
    t.string  "username", limit: 50
    t.string  "Pass",     limit: 50
    t.boolean "status",              default: true
  end

  create_table "UsersRights", id: false, force: true do |t|
    t.integer "userid",      limit: 8,  null: false
    t.string  "Name",        limit: 50
    t.integer "objectid",    limit: 8
    t.boolean "rightaccess"
    t.boolean "rightWrite"
    t.boolean "rightRead"
    t.boolean "rightDelete"
  end

  create_table "UsersRights2", id: false, force: true do |t|
    t.integer "userid",      limit: 8,  null: false
    t.string  "Name",        limit: 50
    t.integer "objectid",    limit: 8
    t.boolean "rightaccess"
    t.boolean "rightWrite"
    t.boolean "rightRead"
    t.boolean "rightDelete"
  end

  create_table "Users_log", force: true do |t|
    t.string   "Username",    limit: 50
    t.text     "Action"
    t.text     "Item_code"
    t.datetime "Action_time"
    t.integer  "Inter_id",    limit: 8,  default: 0
  end

  create_table "XvisionTblRecipe", primary_key: "RepId", force: true do |t|
    t.string  "RecipeCode",       limit: 160, null: false
    t.integer "RecipeNumber",                 null: false
    t.integer "Passes",                       null: false
    t.string  "Alloy",            limit: 160, null: false
    t.float   "CoilWidth",        limit: 53,  null: false
    t.float   "InitialThickness", limit: 53,  null: false
    t.float   "FinalThickness",   limit: 53,  null: false
  end

  create_table "batch_summary", id: false, force: true do |t|
    t.string "Item_code", limit: 50
    t.float  "Batch_qty", limit: 53
    t.float  "Slit_qty",  limit: 53
    t.float  "Blanked",   limit: 53
    t.float  "Annealed",  limit: 53
    t.float  "Packed",    limit: 53
    t.float  "Allo",      limit: 53
    t.float  "Balance",   limit: 53
    t.float  "Rejects",   limit: 53
  end

  create_table "blanking", force: true do |t|
    t.string   "Item_code"
    t.text     "item_description"
    t.datetime "date"
    t.float    "qty",              limit: 53
    t.integer  "month"
    t.integer  "year"
    t.string   "orderno"
    t.string   "username"
    t.string   "Shiftname"
    t.integer  "status",                      default: 0
    t.integer  "pick",                        default: 0
    t.string   "Sourcename"
    t.integer  "excess",                      default: 0
    t.float    "Remaining",        limit: 53
    t.string   "Pressno"
    t.string   "slit_code"
    t.integer  "lotno"
    t.integer  "slitid",           limit: 8
    t.string   "Coilno"
    t.string   "Batchno"
    t.integer  "Transfer",                    default: 0
    t.float    "Rejected",         limit: 53, default: 0.0
  end

  create_table "cnitemse", id: false, force: true do |t|
    t.float  "ctnItemID",     limit: 53
    t.string "ctnItemCode"
    t.string "ctnItemDesc"
    t.string "ctnUOM"
    t.float  "ctnItemMinLev", limit: 53
    t.float  "ctnReOrderLev", limit: 53
  end

  create_table "cntitemtransactions", id: false, force: true do |t|
    t.float    "ctnItemTransID",    limit: 53
    t.float    "ctnItemID",         limit: 53
    t.datetime "ctnTransDate"
    t.float    "ctnTransQttyIssue", limit: 53
    t.float    "ctnTransQttyRecpt", limit: 53
  end

  create_table "cntmodsecurity", id: false, force: true do |t|
    t.string "F1"
    t.float  "recModSecurityID", limit: 53
    t.string "username"
    t.string "allowInsert"
    t.string "allowUpdate"
    t.string "allowDelete"
    t.string "allowView"
    t.float  "moduleID",         limit: 53
  end

  create_table "coil_issue", force: true do |t|
    t.string   "coilno",          limit: 50
    t.float    "coilwidth",       limit: 53
    t.float    "coilbuildup",     limit: 53
    t.float    "qty",             limit: 53
    t.string   "orderno",         limit: 50, default: "N/a"
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.string   "username",        limit: 50
    t.integer  "status",                     default: 0,     null: false
    t.integer  "coilid",          limit: 8
    t.integer  "rolled",                     default: 0
    t.float    "gauge",           limit: 53
    t.float    "WT",              limit: 53
    t.float    "scrap",           limit: 53
    t.string   "rolled_by",       limit: 50
    t.datetime "Rdate"
    t.integer  "Rmonth"
    t.integer  "Ryear"
    t.integer  "Slit",                       default: 0
    t.integer  "Semi_rolled",                default: 0,     null: false
    t.string   "Original_coilno", limit: 50
    t.integer  "batchno",         limit: 8
    t.float    "Final_gauge",     limit: 53
  end

  create_table "coil_stock", force: true do |t|
    t.float    "coilwidth",   limit: 53
    t.float    "gauge",       limit: 53
    t.float    "coilbuildup", limit: 53
    t.float    "qty",         limit: 53
    t.datetime "date"
    t.integer  "month"
    t.integer  "year"
    t.integer  "status",                 default: 0
    t.string   "username",    limit: 50
    t.float    "in_stock",    limit: 53, default: 0.0
    t.integer  "allocated",              default: 0
    t.string   "coilno",      limit: 50
    t.integer  "batchno",     limit: 8
    t.string   "So_no",       limit: 50
  end

  create_table "combined", id: false, force: true do |t|
    t.string  "orderno"
    t.integer "Closed",  default: 1
  end

  create_table "combined1", id: false, force: true do |t|
    t.string  "orderno", limit: 50
    t.integer "closed",             default: 1, null: false
  end

  create_table "current_orderno", id: false, force: true do |t|
    t.string "ORDERNO",   limit: 50
    t.string "ITEM_CODE", limit: 50
    t.string "username",  limit: 50
    t.string "batchno",   limit: 50
    t.string "trayno",    limit: 50
  end

  create_table "daily_rejects", id: false, force: true do |t|
    t.integer "Sno"
    t.string  "coilno",       limit: 50
    t.string  "item_code",    limit: 50
    t.float   "qty",          limit: 53
    t.string  "defect",       limit: 50
    t.string  "customer",     limit: 50
    t.string  "orderno",      limit: 50
    t.string  "machine_name", limit: 50
  end

  create_table "defaults", id: false, force: true do |t|
    t.string "id",      limit: 50
    t.string "default", limit: 160
  end

  create_table "dispatches", id: false, force: true do |t|
    t.string "Code",             limit: 50
    t.text   "Item_description"
    t.string "Invoiceno",        limit: 50
    t.text   "orderno"
    t.float  "Qty",              limit: 53
    t.string "palletno",         limit: 50
  end

  create_table "floor_rejection", primary_key: "ID", force: true do |t|
    t.string   "Item_code",        limit: 50
    t.text     "Item_Description"
    t.string   "Machine_Centre",   limit: 50
    t.float    "Qty",              limit: 53
    t.text     "Reason"
    t.datetime "Date"
    t.integer  "Month"
    t.integer  "Year"
    t.text     "Username"
    t.text     "Customer_Name"
    t.string   "Order_no",         limit: 50
    t.string   "coilno",           limit: 50
    t.string   "Category",         limit: 50
    t.integer  "Recovery",                    default: 0
  end

  create_table "headers", id: false, force: true do |t|
    t.datetime "TIT"
  end

  create_table "inbox", force: true do |t|
    t.string   "username",     limit: 30
    t.string   "msgtype",      limit: 160
    t.string   "msgid",        limit: 60
    t.string   "sender",       limit: 30
    t.string   "receiver",     limit: 30
    t.string   "msgsubject",   limit: 1024
    t.text     "msgdata"
    t.datetime "senttime"
    t.datetime "receivedtime"
    t.string   "operator",     limit: 30
  end

  create_table "list", id: false, force: true do |t|
    t.string "Code",        limit: 50
    t.text   "description"
    t.text   "Customer"
    t.string "Orderno",     limit: 50
    t.float  "qty",         limit: 53
    t.string "Palletno",    limit: 50
  end

  create_table "list_of_despatches", id: false, force: true do |t|
    t.string "orderno",          limit: 50
    t.string "Item_code",        limit: 50
    t.text   "item_description"
    t.float  "qty",              limit: 53
    t.text   "customer"
  end

  create_table "lists", id: false, force: true do |t|
    t.string "code",    limit: 50
    t.text   "Item"
    t.float  "Qty",     limit: 53
    t.string "Station", limit: 50
  end

  create_table "lots", id: false, force: true do |t|
    t.integer "Lotno", limit: 8
  end

  create_table "master", id: false, force: true do |t|
    t.string "Item_description"
    t.string "item_code"
  end

  create_table "mbkGauges", primary_key: "gaugeID", force: true do |t|
    t.float "gauge", limit: 53
  end

  create_table "mbkMachine", primary_key: "machineID", force: true do |t|
    t.string "machineName",        limit: 50
    t.string "machineDescription", limit: 50
  end

  create_table "mbkOutput", primary_key: "outputID", force: true do |t|
    t.integer "machineID"
    t.integer "sizeGaugeID"
    t.float   "outPut",      limit: 53
    t.integer "Priority"
  end

  create_table "mbkSizeGauge", primary_key: "sizeGaugeID", force: true do |t|
    t.integer "sizeID"
    t.integer "gaugeID"
  end

  create_table "mbkSizes", primary_key: "sizeID", force: true do |t|
    t.float "size", limit: 53
  end

  create_table "months", primary_key: "iD", force: true do |t|
    t.string "NAME", limit: 50
  end

  create_table "motherCodeBOM1", primary_key: "bomID", force: true do |t|
    t.integer "motherCodeID"
    t.integer "childID"
    t.float   "childQtty",    limit: 53
  end

  create_table "motherCodeBOM2", primary_key: "BomId", force: true do |t|
    t.integer "motherCodeID"
    t.float   "childQtty",    limit: 53
    t.float   "childID",      limit: 53
  end

  create_table "mtlMaster", primary_key: "rmID", force: true do |t|
    t.string  "rmCode",        limit: 20
    t.string  "rmDescription", limit: 200
    t.integer "rmstoreid"
  end

  create_table "mtlMaster2", id: false, force: true do |t|
    t.string "rmcode"
    t.string " rmDescription"
    t.string "F3"
  end

  create_table "mtnAuditTrail", primary_key: "trailID", force: true do |t|
    t.string   "AuditAction",  limit: 20
    t.string   "AuditDetails", limit: 2000
    t.datetime "auditDate"
    t.datetime "auditTime"
    t.integer  "userID"
  end

  create_table "mtnAutoSMS", primary_key: "autoSMSID", force: true do |t|
    t.integer  "employeeID"
    t.datetime "shiftA"
    t.integer  "shiftAHr"
    t.integer  "shiftAMin"
    t.datetime "ShiftB"
    t.integer  "ShiftBHr"
    t.integer  "ShiftBMin"
    t.datetime "shiftC"
    t.integer  "ShiftCHr"
    t.integer  "ShiftCMin"
    t.datetime "shiftG"
    t.integer  "shiftGHr"
    t.integer  "shiftGMin"
    t.datetime "DailyReport"
    t.integer  "DailyReportHr"
    t.integer  "DailyReportMin"
    t.datetime "ShiftAPicked"
    t.datetime "ShiftBPicked"
    t.datetime "ShiftCPicked"
    t.datetime "ShiftGPicked"
    t.datetime "DailyReportPicked"
  end

  create_table "mtnDomains", primary_key: "domainID", force: true do |t|
    t.string "domainDescription", limit: 50
  end

  create_table "mtnEmployees", primary_key: "employeeID", force: true do |t|
    t.string   "FirstName",          limit: 50
    t.string   "middleName",         limit: 50
    t.string   "surname",            limit: 50
    t.string   "IDNo",               limit: 50
    t.datetime "DateOfBirth"
    t.string   "Gender",             limit: 50
    t.datetime "dateOfRegistration"
    t.string   "EmailAddress",       limit: 50
    t.string   "mobilePhone",        limit: 50
    t.string   "BirthdayMessage",    limit: 160
    t.integer  "NotificationYear"
    t.integer  "BirthDate"
    t.integer  "BirthMonth"
    t.integer  "Msg"
    t.boolean  "IsEmployee"
  end

  create_table "mtnMessagePool", primary_key: "msgID", force: true do |t|
    t.string   "msgCode",                limit: 50
    t.string   "msg",                    limit: 160
    t.integer  "senderID"
    t.integer  "employeeID"
    t.datetime "notificationDate"
    t.boolean  "Picked",                             default: false
    t.datetime "NotificationTime"
    t.integer  "NotificationHourPart"
    t.integer  "NotificationMinutePart"
  end

  create_table "mtnModules", id: false, force: true do |t|
    t.integer "moduleID",                     null: false
    t.string  "moduleDescription", limit: 50
  end

  create_table "mtnMsgCode", primary_key: "msgCodeID", force: true do |t|
    t.integer "msgCode"
  end

  create_table "mtnSMSCompletePallet", primary_key: "palletID", force: true do |t|
    t.string   "so_no",      limit: 50
    t.float    "Qty",        limit: 53
    t.float    "pltQty",     limit: 53
    t.datetime "transDate"
    t.integer  "transMonth"
    t.integer  "transYear"
  end

  create_table "mtnServerParams", primary_key: "serverParamID", force: true do |t|
    t.datetime "serverDate",              null: false
    t.integer  "serverHour"
    t.integer  "serverMinute"
    t.string   "entryDate",    limit: 50
  end

  create_table "mtnUserDomain", primary_key: "userDomainID", force: true do |t|
    t.integer "userID"
    t.integer "domainID"
  end

  create_table "mtnUserRights", primary_key: "userRightID", force: true do |t|
    t.integer "domainID"
    t.integer "moduleID"
    t.boolean "r_Add"
    t.boolean "r_Delete"
    t.boolean "r_Update"
    t.boolean "r_View"
  end

  create_table "mtnUsers", primary_key: "userID", force: true do |t|
    t.string   "username",         limit: 50
    t.integer  "employeeID",                  default: 0
    t.string   "password",         limit: 50
    t.boolean  "Disabled",                    default: false
    t.boolean  "SMSSelf",                     default: true
    t.boolean  "EnableBirthTimer",            default: true
    t.boolean  "EnableTransTimer",            default: true
    t.datetime "Expiry"
  end

  create_table "obform", primary_key: "Obid", force: true do |t|
    t.string "name", limit: 50
  end

  create_table "obform2", primary_key: "Obid", force: true do |t|
    t.string "name", limit: 50
  end

  create_table "openningstock", force: true do |t|
    t.string "item_code"
    t.string "Item_description"
    t.float  "qty",              limit: 53
    t.float  "month",            limit: 53
    t.float  "year",             limit: 53
    t.string "category"
  end

  create_table "openningstock2", id: false, force: true do |t|
    t.string "Item_code"
    t.string "Item_description"
    t.float  "QTY",              limit: 53
    t.float  "month",            limit: 53
    t.float  "year",             limit: 53
    t.string "Category"
  end

  create_table "openningstock20130916", force: true do |t|
    t.string "item_code"
    t.string "Item_description"
    t.float  "QTY",              limit: 53
    t.float  "month",            limit: 53
    t.float  "year",             limit: 53
    t.string "Category"
  end

  create_table "openningstock3", id: false, force: true do |t|
    t.string "Item_code"
    t.string "Item_description"
    t.float  "QTY",              limit: 53
    t.float  "month",            limit: 53
    t.float  "year",             limit: 53
    t.string "Category"
    t.string "id",               limit: 10
  end

  create_table "openningstock4", force: true do |t|
    t.string "Item_code"
    t.string "Item_description"
    t.float  "QTY",              limit: 53
    t.float  "month",            limit: 53
    t.float  "year",             limit: 53
    t.string "Category"
  end

  create_table "orderstatus", id: false, force: true do |t|
    t.string "Item_code",        limit: 50
    t.text   "Item_description"
    t.float  "Qty",              limit: 53
    t.float  "Packed",           limit: 53
    t.float  "FG",               limit: 53
    t.float  "Pallets",          limit: 53
    t.float  "UNALL",            limit: 53
    t.float  "ANN",              limit: 53
    t.float  "SOFT",             limit: 53
    t.float  "HD",               limit: 53
    t.float  "UNHD",             limit: 53
    t.float  "STK",              limit: 53
  end

  create_table "outbox", force: true do |t|
    t.string   "username",                limit: 30
    t.string   "msgtype",                 limit: 160
    t.string   "msgid",                   limit: 60
    t.string   "callbackid"
    t.string   "sender",                  limit: 30
    t.string   "receiver",                limit: 30
    t.string   "msgsubject",              limit: 1024
    t.text     "msgdata"
    t.datetime "acceptedfordeliverytime"
    t.datetime "deliveredtonetworktime"
    t.datetime "deliveredtohandsettime"
    t.string   "operator",                limit: 30
    t.string   "route",                   limit: 30
    t.string   "status",                  limit: 130
    t.string   "errormessage",            limit: 1024
    t.string   "cost",                    limit: 10
    t.datetime "FormularDate",                         null: false
  end

  create_table "ozekimessagein", force: true do |t|
    t.string "sender",       limit: 30
    t.string "receiver",     limit: 30
    t.string "msg",          limit: 160
    t.string "senttime",     limit: 100
    t.string "receivedtime", limit: 100
    t.string "operator",     limit: 30
    t.string "msgtype",      limit: 30
    t.string "reference",    limit: 30
  end

  create_table "ozekimessageout", force: true do |t|
    t.string   "sender",       limit: 30
    t.string   "receiver",     limit: 30
    t.string   "msg",          limit: 160
    t.datetime "senttime"
    t.string   "receivedtime", limit: 100
    t.string   "operator",     limit: 100
    t.string   "msgtype",      limit: 30
    t.string   "reference",    limit: 30
    t.string   "status",       limit: 30
    t.string   "errormsg",     limit: 250
  end

  create_table "ozekimessageout_20140118", force: true do |t|
    t.string   "sender",       limit: 30
    t.string   "receiver",     limit: 30
    t.string   "msg",          limit: 160
    t.datetime "senttime"
    t.string   "receivedtime", limit: 100
    t.string   "operator",     limit: 100
    t.string   "msgtype",      limit: 30
    t.string   "reference",    limit: 30
    t.string   "status",       limit: 30
    t.string   "errormsg",     limit: 250
  end

  create_table "packing", primary_key: "ID", force: true do |t|
    t.string   "Item_code"
    t.string   "Item_description"
    t.datetime "date"
    t.string   "coil_number",      limit: 50
    t.float    "qty",              limit: 53
    t.integer  "month"
    t.integer  "year"
    t.string   "station"
    t.integer  "status",                      default: 0
    t.string   "Orderno"
    t.string   "username"
    t.string   "Shiftname"
    t.integer  "orderwise"
    t.integer  "pallet",                      default: 0
    t.integer  "Transfer",                    default: 1
    t.string   "Batchno",          limit: 50
    t.string   "Category",         limit: 50
    t.string   "Customer"
    t.string   "item_code1",       limit: 50
    t.float    "TOTAL_WT_PC",      limit: 53
  end

  create_table "packing_20130418", primary_key: "ID", force: true do |t|
    t.string   "Item_code"
    t.string   "Item_description"
    t.datetime "date"
    t.string   "coil_number",      limit: 50
    t.float    "qty",              limit: 53
    t.integer  "month"
    t.integer  "year"
    t.string   "station"
    t.integer  "status"
    t.string   "Orderno"
    t.string   "username"
    t.string   "Shiftname"
    t.integer  "orderwise"
    t.integer  "pallet"
    t.integer  "Transfer"
    t.string   "Batchno",          limit: 50
    t.string   "Category",         limit: 50
    t.string   "Customer"
    t.string   "item_code1",       limit: 50
  end

  create_table "packingrate", force: true do |t|
    t.float   "Rate",      limit: 53
    t.integer "Autono"
    t.integer "sysdate"
    t.boolean "uselimit"
    t.integer "pallets",   limit: 8
    t.integer "max_stock", limit: 8
  end

  create_table "palletlist", id: false, force: true do |t|
    t.string "Item_code",        limit: 50
    t.text   "Item_description"
    t.float  "WT",               limit: 53
    t.string "Orderno",          limit: 50
  end

  create_table "pallets_20130713", force: true do |t|
    t.string   "Item_code"
    t.text     "Item_description"
    t.string   "Palletno"
    t.float    "QTY",              limit: 53
    t.datetime "Date"
    t.integer  "MONTH"
    t.integer  "YEAR"
    t.text     "Customer"
    t.string   "Orderno"
    t.integer  "status"
    t.integer  "dispatch"
    t.string   "Category"
    t.integer  "packid",           limit: 8
    t.integer  "locid",            limit: 8
    t.string   "username",         limit: 50
    t.integer  "Transfer"
    t.integer  "salid",            limit: 8
    t.string   "item_type",        limit: 50
  end

  create_table "portaluser", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "portaluser", ["email"], name: "index_portaluser_on_email", unique: true
  add_index "portaluser", ["reset_password_token"], name: "index_portaluser_on_reset_password_token", unique: true

  create_table "processRoute", force: true do |t|
    t.string  "itemCategory",     limit: 50
    t.string  "processRouteName", limit: 5000
    t.integer "status",                        default: 0
    t.string  "userName",         limit: 50
  end

  create_table "processRouteCard", force: true do |t|
    t.string "processName", limit: 50
    t.string "startDate",   limit: 50
    t.string "endDate",     limit: 50
    t.string "qtyIn",       limit: 50
    t.string "qtyOut",      limit: 50
    t.string "userName",    limit: 50
    t.string "customer",    limit: 50
    t.float  "requiredQty", limit: 53
    t.string "commitDate",  limit: 50
  end

  create_table "remelt", primary_key: "ID", force: true do |t|
    t.string   "Item_code",        limit: 50
    t.text     "Item_description"
    t.string   "coilnumber",       limit: 50
    t.datetime "date"
    t.float    "qty",              limit: 53
    t.integer  "month"
    t.integer  "year"
    t.text     "reasons"
  end

  create_table "rmCenterMaster", primary_key: "centerID", force: true do |t|
    t.string "centerName", limit: 100
  end

  create_table "rmNumbering", id: false, force: true do |t|
    t.integer "nextNum", limit: 8
  end

  create_table "rmRequest", primary_key: "rmReqID", force: true do |t|
    t.datetime "rmReqDate"
    t.integer  "rmID"
    t.integer  "centerID"
    t.float    "Quantity",    limit: 53
    t.string   "createdBy",   limit: 50
    t.string   "rmReqNo",     limit: 50
    t.boolean  "rmReqStatus",            default: false
    t.boolean  "Approve",                default: false
  end

  create_table "rmRequestTransactions", primary_key: "rmTransactionID", force: true do |t|
    t.string   "rmReqNo",   limit: 50
    t.integer  "rmID"
    t.float    "Quantity",  limit: 53
    t.integer  "centerID"
    t.datetime "transDate"
    t.string   "username",  limit: 50
    t.float    "Box",       limit: 53, default: 0.0
    t.float    "Net",       limit: 53, default: 0.0
    t.integer  "status",               default: 0
  end

  create_table "salesorders", primary_key: "ID", force: true do |t|
    t.decimal "orderno",    precision: 18, scale: 0
    t.integer "customerid"
  end

  create_table "schedule", id: false, force: true do |t|
    t.string "orderno",          limit: 50
    t.text   "Item_description"
    t.float  "Qty_required",     limit: 53
    t.float  "qty_issued",       limit: 53
    t.float  "qty_remaining",    limit: 53
    t.string "status",           limit: 50
  end

  create_table "scrap", id: false, force: true do |t|
    t.string "Pressno", limit: 50
    t.float  "Input",   limit: 53
    t.float  "output",  limit: 53
    t.float  "scrap",   limit: 53
  end

  create_table "slit_master", force: true do |t|
    t.string "slit_code"
    t.string "slit_description"
    t.float  "Dia",              limit: 53
    t.string "Category"
  end

  create_table "slitter_input", force: true do |t|
    t.string   "coilno", limit: 50
    t.float    "qty",    limit: 53
    t.datetime "date"
  end

  create_table "smsMONITOR", primary_key: "transID", force: true do |t|
    t.datetime "transDate"
    t.float    "pltQtty",   limit: 53
  end

  create_table "source_Centre", primary_key: "ID", force: true do |t|
    t.string "Name", limit: 50
  end

  create_table "stock_list", id: false, force: true do |t|
    t.string "Item_code",        limit: 50
    t.text   "item_description"
    t.float  "qty",              limit: 53
  end

  create_table "stocklevels", id: false, force: true do |t|
    t.string  "code",            limit: 50
    t.decimal "Dia",                        precision: 18, scale: 0
    t.decimal "thickness",                  precision: 18, scale: 0
    t.string  "Temp",            limit: 10
    t.float   "Opstk",           limit: 53
    t.float   "Packed",          limit: 53
    t.float   "qty",             limit: 53
    t.float   "Pallets_qty",     limit: 53
    t.float   "Unallocated_qty", limit: 53
  end

  create_table "sysdiagrams", primary_key: "diagram_id", force: true do |t|
    t.string  "name",         limit: 128, null: false
    t.integer "principal_id",             null: false
    t.integer "version"
    t.binary  "definition"
  end

  add_index "sysdiagrams", ["principal_id", "name"], name: "UK_principal_name", unique: true

  create_table "tblMotherCodeRemarks", primary_key: "remarkID", force: true do |t|
    t.string "Remarks", limit: 50
    t.float  "weight",  limit: 53
  end

  create_table "tblOrderstatus", primary_key: "Id", force: true do |t|
    t.string "ItemCode",        limit: 160, null: false
    t.string "ItemDescription", limit: 300, null: false
    t.float  "Balance",         limit: 53,  null: false
    t.float  "Allocated",       limit: 53,  null: false
    t.float  "OrderedQty",      limit: 53,  null: false
    t.date   "Commitdate"
    t.float  "PalletisedQty",   limit: 53,  null: false
    t.string "OrderNo",         limit: 160, null: false
    t.string "Customer",        limit: 300, null: false
    t.string "BatchNum",        limit: 160
  end

  create_table "tblStation", primary_key: "station_id", force: true do |t|
    t.string "station_name",        limit: 50
    t.string "station_description", limit: 50
  end

  create_table "titles", id: false, force: true do |t|
    t.text     "Name"
    t.datetime "Date"
  end

  create_table "username", id: false, force: true do |t|
    t.float    "userID",      limit: 53
    t.string   "userName"
    t.string   "passwd"
    t.datetime "accExpiry"
    t.string   "accDisabled"
    t.string   "FullNames"
  end

  create_table "weighing_scale_setting", primary_key: "MachineId", force: true do |t|
    t.string  "machine",       limit: 50
    t.string  "Portname",      limit: 50
    t.integer "Handshake"
    t.integer "Parity"
    t.integer "Stopbit"
    t.integer "databit"
    t.integer "readbuffer"
    t.integer "baudrate"
    t.integer "timerinterval"
  end

  create_table "wip", primary_key: "ID", force: true do |t|
    t.string   "item_code"
    t.string   "Item_description"
    t.string   "Coilno"
    t.datetime "Date"
    t.float    "Qty",              limit: 53
    t.integer  "month"
    t.integer  "year"
    t.integer  "status",                      default: 0
    t.string   "orderno",          limit: 50
    t.string   "category",         limit: 50
    t.integer  "processid"
  end

  create_table "xvisionTblRecipeDetails", primary_key: "Id", force: true do |t|
    t.integer "RecipeNumber",                 null: false
    t.string  "RecipeCode",       limit: 160, null: false
    t.integer "StepNumber",                   null: false
    t.float   "Coilwidth",        limit: 53,  null: false
    t.float   "InitialThickness", limit: 53,  null: false
    t.float   "FinalThickness",   limit: 53,  null: false
    t.float   "TensSpIn",         limit: 53,  null: false
    t.float   "TensSpOut",        limit: 53,  null: false
    t.float   "TensIn",           limit: 53,  null: false
    t.float   "TensOut",          limit: 53,  null: false
    t.float   "ReductionPercent", limit: 53,  null: false
    t.float   "Speed",            limit: 53,  null: false
  end

end
