class OrdersController < ApplicationController
  before_filter :authenticate_user!
  # GET /orders
  # GET /orders.json
  def index
    begin
      @search=Order.search(params[:q])
      @orders=@search.result.order('orderdate').where('email=?' ,current_user.email).paginate(page: params[:page],per_page: 10)
    rescue ActiveRecord::RecordNotFound
      redirect_to root_url,:alert=>'Invalid request'
    end
  end
  # GET /orders/1
  # GET /orders/1.json
private 
def order_params
      params.require(:order).permit(:pono,:order_id)
 end
end
