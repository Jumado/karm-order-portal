class RegistrationsController <  Devise::RegistrationsController
  before_filter :authenticate_user!, :only => :token
  before_action :confirm_user,:only =>:create
  def new
      super
    end
  def create
    if (@user.count>0)
      super
    else
      redirect_to new_user_registration_path,:alert=>"provided mail is not registered with us"
    end
  end
  def update
    super
  end
private
  def confirm_user
    @user=User.connection.exec_query("SELECT Emailaddress FROM Customers_emailaddress WHERE Emailaddress='#{params[:user][:email]}'")
  end
end
