class ItemsController < ApplicationController
  before_filter :authenticate_user!
  # GET /items
  # GET /items.json
  def index
    begin
     @items=Item.search(params[:search]).order('itemcode').where("pono=? and email=?",params[:pono],current_user.email).paginate(page: params[:page],per_page: 10)
     respond_to do |format| 
       format.js()
       format.html()
       flash[:notice]= "P.O NUMBER #{params[:pono]}"
     end
    rescue ActiveRecord::RecordNotFound
      redirect_to root_url,:alert=>'Invalid request'
    end
  end
    private
    # Use callbacks to share common setup or constraints between actions.
    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:pono, :description, :itemcode,:order_id,:email,:search)
    end
end
