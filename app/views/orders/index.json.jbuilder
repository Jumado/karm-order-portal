json.array!(@orders) do |order|
  json.extract! order, :id, :customer, :orderno, :itemcode
  json.url order_url(order, format: :json)
end
